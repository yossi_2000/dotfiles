"shell がfishだとPOSIX対応ではないので標準のshにする
if &shell =~# 'fish$'
    set shell=sh
endif

"viとの互換性を無効にする(INSERT中にカーソルキーが有効になる)
set nocompatible
"カーソルを行頭，行末で止まらないようにする
set whichwrap=b,s,h,l,<,>,[,]
"BSで削除できるものを指定する
" indent  : 行頭の空白
" eol     : 改行
" start   : 挿入モード開始位置より手前の文字
set backspace=indent,eol,start

"indent を考慮して折りたたみ
set foldmethod=indent  "折りたたみ範囲の判断基準（デフォルト: manual）
set foldlevel=2        "ファイルを開いたときにデフォルトで折りたたむレベル
set foldcolumn=3       "左端に折りたたみ状態を表示する領域を追加する


"タイトル表示
set title			"タイトル表示
set autoindent
set tabstop=4		
set ignorecase
set smartcase
set background=dark	"暗くする
set number			"行番号をふる
set smartindent		"自動インデント
set completeopt=menuone

"python3
set pyxversion=3
set pyx=3



"カーソル移動
source $VIMRUNTIME/macros/matchit.vim " Vimの「%」を拡張す

"自動的にIMEをオフにする
	set iminsert=0
	set imsearch=-1

" 現在の行に、下線を表示する
	set cursorline


"set mouse=a			"マウス


"最後のカーソル位置を復元する
augroup vimrcEx
  au BufRead * if line("'\"") > 0 && line("'\"") <= line("$") |
  \ exe "normal g`\"" | endif
augroup END


"deinの設定------------------------------------------------------------
" プラグインが実際にインストールされるディレクトリ
let s:dein_dir = expand('~/.config/vim/dein')
" dein.vim 本体
let s:dein_repo_dir = s:dein_dir . '/repos/github.com/Shougo/dein.vim'

" dein.vim がなければ github から落としてくる
if &runtimepath !~# '/dein.vim'
  if !isdirectory(s:dein_repo_dir)
    execute '!git clone https://github.com/Shougo/dein.vim' s:dein_repo_dir
  endif
  execute 'set runtimepath^=' . fnamemodify(s:dein_repo_dir, ':p')
endif

if dein#load_state(s:dein_dir)
  call dein#begin(s:dein_dir)

  " プラグインリストを収めた TOML ファイル
  " 予め TOML ファイル（後述）を用意しておく
  let g:rc_dir    = expand('~/.config/vim/rc')
  let s:toml      = g:rc_dir . '/dein.toml'
  let s:lazy_toml = g:rc_dir . '/dein_lazy.toml'

  " TOML を読み込み、キャッシュしておく
  call dein#load_toml(s:toml,      {'lazy': 0})
  call dein#load_toml(s:lazy_toml, {'lazy': 1})
" 設定終了
  call dein#end()
  call dein#save_state()
endif

" もし、未インストールものものがあったらインストール
if dein#check_install()
  call dein#install()
endif


if dein#tap('deoplete.nvim')
  let g:deoplete#enable_at_startup = 1
endif

"python
if &filetype=="python"
	set tabstop=8
endif

" 対応する括弧やブレースを表示
	set showmatch matchtime=1

"jjで<ESC>にする
	inoremap <silent> jj <ESC>

"Ctr-nでnerdtree
	map <silent> <c-n> :NERDTreeFocus<CR>

"分割
  nnoremap sj <C-w>j
  nnoremap sk <C-w>k
  nnoremap sl <C-w>l
  nnoremap sh <C-w>h
  nnoremap <silent> sv :vsplit<CR>
  nnoremap <silent> ss :split<CR>


nnoremap <silent> <Space>w :<C-u>w<CR>
nnoremap <silent> <Space>q :<C-u>q<CR>



"コードの色分け
	syntax on
	filetype indent on

"自動改行
	set display=lastline

"下のメニュー表示と表示内容
	set laststatus=2
	set statusline=%F%r%h%=


"deoplete

if dein#tap('deoplete.nvim')
	let g:deoplete#enable_at_startup = 1
	let g:deoplete#auto_completion_start_length = 1
endif


""
"" vimtex
""
let g:vimtex_latexmk_continuous = 1
let g:vimtex_latexmk_options = '-pdfdvi'
let g:vimtex_view_general_viewer = 'open'
 nnoremap <C-x>t :VimtexCompile<Enter>

	let twitvim_browser_cmd = 'open' " for Mac
	let twitvim_force_ssl = 1 
	let twitvim_count = 100


"vimtex
	let g:vimtex_compiler_latexmk_engines = { '_' : '-pdfdvi'} 

function! s:clang_format()
	let now_line = line(",")
	exec ":%! clang-format"
	exec ":" . now_line
endfunction

if executable('clang-format')
	augroup cpp_clang_format
		autocmd!
		autocmd BufWrite,FileWritePre,FileAppendPre *.[ch]pp call s:clang_format()
	augroup END
endif
