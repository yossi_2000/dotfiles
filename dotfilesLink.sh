#!/bin/bash

if [ "$(uname)" = 'Darwin' ]; then
    echo "mac"
	ln -sf ~/dot_files/.vimrc ~/.vimrc
    ln -sf ~/dot_files/.bashrc ~/.bashrc
    ln -sf ~/dot_files/.gitconfig ~/.gitconfig
    ln -sf ~/dot_files/.zshrc ~/.zshrc
	ln -sf ~/dot_files/vim/dein.toml ~/.config/vim/rc/dein.toml
	ln -sf ~/dot_files/vim/dein_lazy.toml ~/.config/vim/rc/dein_lazy.toml
elif [ "$(expr substr $(uname -s) 1 5)" = 'Linux' ]; then
	echo "Linux"
    ln -snf ~/dot_files/fish ~/.config/fish
    ln -sf ~/dot_files/git_files/.gitconfig ~/.gitconfig
	ln -sf ~/dot_files/tmux/.tmux.conf ~/.tmux.conf

	mkdir -p ~/.config/vim/rc
    ln -sf ~/dot_files/.vimrc ~/.vimrc
    ln -sf ~/dot_files/vim/dein.toml ~/.config/vim/rc/dein.toml
    ln -sf ~/dot_files/vim/dein_lazy.toml ~/.config/vim/rc/dein_lazy.toml
fi
