set os_name (uname)

if test $os_name = "Darwin"
		set -x PATH /usr/local/opt/make/libexec/gnubin $PATH

set -x PATH /usr/local/opt/gnu-sed/libexec/gnubin $PATH
set -x MANPATH /usr/local/opt/gnu-sed/libexec/gnuman gMANPATH
set -x PATH /usr/local/opt/openssl/bin $PATH
set -x SHELL /usr/local/bin/fish
set -x PATH /usr/local/Homebrew/Library/sims/scm $PATH

set -x PATH /usr/local/texlive/2018/bin/x86_64-darwin $PATH
	echo Darwin
	set not_os_name l
	alias ls="ls -AFG"
	alias line='open/Applications/Line.app'

else if test $os_name = "Linux"
	set not_os_name m
	alias ls='ls -AFG --color=auto'
	alias sl='sl -e'
	alias LS='sl -e'


else if test $os_name = "Windows"
	echo "Windows is invalid!"
	exit 1
else 
	echo "unknown os!"
	exit 1
end
#echo "end of judging os "

for file_path in ~/dot_files/fish/alias/*.fish
	if  test (string sub -s 1 -l 1 (string  split -rm1 / $file_path)[2] ) = $not_os_name 
		continue
	end
	#echo $file_path
	source $file_path
end


for file_path in ~/dot_files/fish/function/*.fish
	if  test (string sub -s 1 -l 1 (string  split -rm1 / $file_path)[2] ) = $not_os_name 
		continue
	end
	#echo $file_path
	source $file_path
end
