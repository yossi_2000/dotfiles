function fish_prompt 
	
    if set -l git_branch (command git symbolic-ref HEAD 2>/dev/null | string replace refs/heads/ '')
        
        if command git diff-index --quiet HEAD --
            if set -l count (command git rev-list --count --left-right $upstream...HEAD 2>/dev/null)
                echo $count | read -l ahead behind
                if test "$ahead" -gt 0
                    set git_status "$git_status"(set_color red)⬆
                end
                if test "$behind" -gt 0
                    set git_status "$git_status"(set_color red)⬇
                end
            end
            for i in (git status --porcelain | string sub -l 2 | uniq)
                switch $i
                    case "."
                        set git_status "$git_status"(set_color green)
                    case " D"
                        set git_status "$git_status"(set_color red)
                    case "*M*"
                        set git_status "$git_status"(set_color green)
                    case "*R*"
                        set git_status "$git_status"(set_color purple)
                    case "*U*"
                        set git_status "$git_status"(set_color brown)
                    case "??"
                        set git_status "$git_status"(set_color red)
                end
            end
        else
            set git_status (set_color green):
        end
        set git_info "($git_status$git_branch"(set_color white)")"
    end
    set -l last_status $status

    if test $last_status -ne 0
        printf "%s(%d)%s " (set_color red --bold) $last_status (set_color normal)
    end

    set -l color_cwd
    set -l suffix
    switch "$USER"
        case root toor
            if set -q fish_color_cwd_root
                set color_cwd $fish_color_cwd_root
            else
                set color_cwd $fish_color_cwd
            end
            set suffix '#'
        case '*'
            set color_cwd $fish_color_cwd
            set suffix '>'
    end

    if test -z  $SSH_CLIENT 
        set ssh_info ""
    else 
        set ssh_info (echo $SSH_CLIENT |string split \ | awk 'NR==1')                  │
    end


    echo -n -s (set_color cyan)$ssh_info(whoami)" @ "(set_color normal)(echo $PWD | sed -e "s|^$HOME|~|" | string split / | tac | awk 'NR==1;NR>1{print substr($0,length($0),length($0))}' | tac | string join / ) $git_info (set_color red)' ❯'(set_color yellow)'❯'(set_color green)'❯ '(set_color normal)
end
