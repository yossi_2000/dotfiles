function twitter

	argparse -n twitter 'p/post' 'w/watch' -- $argv
	or return 1

	if set -lq _flag_post
		set post_flag true
	else 
		set post_flag false
	end

	if set -lq _flag_watch
		set watch_flag true
	else 
		set post_flag false
	end

	if eval $watch_flag 	
		set command "-c FriendsTwitter"
	end

	if eval $post_flag 
		set command  "-c PosttoTwitter"
	end

	if set -lq $command
			return 1
	end

	echo "vim " $command
end
