function LaTeX
	
	argparse -n LaTeX 'o/open' 'y/yen' -- $argv 
	or return 1

	if set -lq _flag_open
		set open_flag true
	end

	if set -lq _flag_sed
		set sed_flag true
	end

	#引数の処理
	if test ( count $argv )  -eq 0 ;and eval $open_flag
		cd ~/program/LaTex/
		return 0
	else if test ( count $argv ) -ne 1 ;
		echo '引数の個数が不正です。'
		return 1
	end

	set name (string split -rm1 . $argv)[1]
	#echo $name".tex"
	if not test -f $name.tex
			echo $name".tex は存在しません!"
		return 0
	end		#end of 引数の処理

	if test -f $name.tex_yen; and eval $sed_flag
		cat $name.tex_yen | sed -e "s#\¥#\\\#g" > $name.tex; or return 0
	end

	#ebb
	for file in (find . -maxdepth 1 -name '*.png')
    	ebb $file
	end		#end of ebb
	

	#dirの移動
	if  test (string split -rm1 / (pwd))[2] != 'build' 
		set move_flag true
				if test -d build/ 
			cd build/
		else if test -f build 
			echo 'buildファイルがあるのでfailしました。'
			exit 1
		else 
			mkdir build ; and cd build
		end
	else
		set move_flag false
	end		#end of dir

	#latexmk
	if eval $move_flag
		latexmk ../$name.tex 1>/dev/null
	else
		latexmk $name.tex 1>/dev/null
	end		#end of latexmk

	#return to dir
	if eval $move_flag
		cd ../
	end		#end of return to dir

	# open pdf
	if eval $open_flag
		open $name.pdf
	end

end #end of func LaTeX
