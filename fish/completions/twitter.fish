function __fish_twitter_need_command
	set cmd (commandline -opc )
	if [ ( count $cmd ) -eq 1 ]
		return 0
	else
		return 1
	end
end

# https://ss64.com/osx/chflags.html

complete -f -c twitter -n '__fish_twitter_need_command' -s p -a post -d 'set the hidden flag'
complete -f -c twitter -n '__fish_twitter_need_command' -s w -a watch -d "watch Friends Twitter"
