set -x VOYAGER_LIGHTHOUSE_STREAM off
set -x PATH /usr/local/bin $PATH

ln -sf dot_files/git_files/.gitconfig ~/.gitconfig


#.fishのlistの読み込み
if test -e ~/dot_files/fish/source.fish
	source ~/dot_files/fish/source.fish
end
